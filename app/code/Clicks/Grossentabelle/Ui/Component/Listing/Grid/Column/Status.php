<?php

namespace Clicks\Grossentabelle\Ui\Component\Listing\Grid\Column;

use Magento\Framework\Option\ArrayInterface;

class Status implements ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 1, 'label' => __('Aktiv')], ['value' => 0, 'label' => __('Inaktiv')]];
    }
}