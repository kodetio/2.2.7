<?php

namespace Clicks\Grossentabelle\Setup;

use Magento\Backend\Block\Widget\Tab;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('clicks_grossentabelle_tables')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('clicks_grossentabelle_tables')
            )
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                        'auto_increment' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    1024,
                    [],
                    'Table description'
                )
                ->addColumn(
                    'sort_order',
                    Table::TYPE_INTEGER,
                    null,
                    [],
                    'Sorting'
                )
                ->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    1024,
                    [],
                    'Table description'
                )
                ->addColumn(
                    'infotext',
                    Table::TYPE_TEXT,
                    10000,
                    [],
                    'Custom text for table'
                )
                ->addColumn(
                    'graphics',
                    Table::TYPE_TEXT,
                    10000,
                    [],
                    'Custom design for table'
                )
                ->addColumn(
                    'graphics_infotext',
                    Table::TYPE_TEXT,
                    10000
                )
                ->addColumn(
                    'attribute_mapping',
                    Table::TYPE_TEXT,
                    10000,
                    [],
                    'Attribute mapping (serialized array)'
                )
                ->setComment('Stores general (fallback) size tables');
    
            $installer->getConnection()->createTable($table);
        }
        if (!$installer->tableExists('clicks_grossentabelle_brands')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('clicks_grossentabelle_brands')
            )
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                        'auto_increment' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    1024,
                    [],
                    'Table description'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_BOOLEAN,
                    null,
                    [],
                    'Status'
                )
                ->addColumn(
                    'url',
                    Table::TYPE_TEXT,
                    1024,
                    [],
                    'URL'
                )
                ->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    1024,
                    [],
                    'Table description'
                )
                ->addColumn(
                    'logo',
                    Table::TYPE_TEXT,
                    1024,
                    [],
                    'Logo'
                )
                ->setComment('Stores brands');
    
            $installer->getConnection()->createTable($table);
        }
        
        $installer->endSetup();
    }
}