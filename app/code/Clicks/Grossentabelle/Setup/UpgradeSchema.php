<?php

namespace Clicks\Grossentabelle\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            if (!$installer->tableExists('clicks_grossentabelle_brands_tables')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('clicks_grossentabelle_brands_tables')
                )
                    ->addColumn(
                        'id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'nullable' => false,
                            'primary' => true,
                            'unsigned' => true,
                            'auto_increment' => true
                        ],
                        'ID'
                    )
                    ->addColumn(
                        'brand_id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'unsigned' => true
                        ],
                        'Brand ID'
                    )
                    ->addColumn(
                        'anchor',
                        Table::TYPE_TEXT,
                        null,
                        [
                            'unsigned' => true
                        ],
                        'Anchor'
                    )
                    ->addColumn(
                        'name',
                        Table::TYPE_TEXT,
                        1024,
                        [],
                        'Table description'
                    )
                    ->addColumn(
                        'sort_order',
                        Table::TYPE_INTEGER,
                        null,
                        [],
                        'Sorting'
                    )
                    ->addColumn(
                        'description',
                        Table::TYPE_TEXT,
                        1024,
                        [],
                        'Table description'
                    )
                    ->addColumn(
                        'infotext',
                        Table::TYPE_TEXT,
                        10000,
                        [],
                        'Custom text for table'
                    )
                    ->addColumn(
                        'graphics',
                        Table::TYPE_TEXT,
                        10000,
                        [],
                        'Custom design for table'
                    )
                    ->addColumn(
                        'graphics_infotext',
                        Table::TYPE_TEXT,
                        10000
                    )
                    ->addColumn(
                        'attribute_mapping',
                        Table::TYPE_TEXT,
                        10000,
                        [],
                        'Attribute mapping (serialized array)'
                    )
                    ->addForeignKey(
                        $installer->getFkName(
                            'clicks_grossentabelle_brands_tables',
                            'brand_id',
                            'clicks_grossentabelle_brands',
                            'id'
                        ),
                        'brand_id',
                        $installer->getTable('clicks_grossentabelle_brands'),
                        'id',
                        Table::ACTION_CASCADE
                    )
                    ->setComment('Stores size tables for given brands');
        
                $installer->getConnection()->createTable($table);
            }
        }
        
        $installer->endSetup();
    }
}