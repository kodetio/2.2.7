<?php

namespace Clicks\Grossentabelle\Block;

use Clicks\Grossentabelle\Model\ImageUploader;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;

class Display extends Template
{
    /** @var \Magento\Framework\App\Config\ScopeConfigInterface $_scopeConfig */
    protected $_scopeConfig;
    
    /** @var \Clicks\Grossentabelle\Model\ImageUploader $_imageUploader */
    private $_imageUploader;
    
    public function __construct(
        ImageUploader $imageUploader,
        ScopeConfigInterface $scopeConfig,
        Template\Context $context,
        array $data = []
    ) {
        $this->_imageUploader = $imageUploader;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }
    
    /**
     * @return string
     */
    public function getBaseImagePath()
    {
        return $this->_imageUploader->baseTmpPath;
    }
    
    /**
     * @return \Clicks\Grossentabelle\Model\Brands | null
     */
    public function getBrand()
    {
        return $this->getRequest()->getParam('brand');
    }
    
    public function _prepareLayout() {
        /** @var \Clicks\Grossentabelle\Model\Brands $brand */
        $brand = $this->getBrand();
        /** @var \Magento\Theme\Block\Html\Breadcrumbs $breadcrumbs */
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        $brandsUrl = $this->_scopeConfig->getValue('brands_fallback/config/url', ScopeInterface::SCOPE_STORE);
    
        // generate dynamic breadcrumbs
        $breadcrumbs
            ->addCrumb('Home', ['link' => '/', 'label' => 'Home', 'title'=> 'Home'])
            ->addCrumb('Brands', ['link' => $brandsUrl, 'label' => __('Marken'), 'title'=> __('Marken')])
            ->addCrumb('Test', ['link' => $brand->getUrl(), 'label' => $brand->getName(), 'title'=> $brand->getName()]);
        $this->getLayout()->setBlock('breadcrumbs', $breadcrumbs);
        
        return parent::_prepareLayout();
    }
}