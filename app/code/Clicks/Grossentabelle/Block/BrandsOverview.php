<?php

namespace Clicks\Grossentabelle\Block;

use Clicks\Grossentabelle\Model\ImageUploader;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;

class BrandsOverview extends Template
{
    
    /** @var \Clicks\Grossentabelle\Model\ResourceModel\Brands\CollectionFactory $_brandsCollection */
    private $_brandsCollection;
    
    /** @var ScopeConfigInterface $scopeConfig */
    protected $_scopeConfig;
    
    /** @var \Clicks\Grossentabelle\Model\ImageUploader $_imageUploader */
    private $_imageUploader;
    
    public function __construct(
        ImageUploader $imageUploader,
        \Clicks\Grossentabelle\Model\ResourceModel\Brands\CollectionFactory $collectionFactory,
        ScopeConfigInterface $scopeConfig,
        Template\Context $context,
        array $data = []
    ) {
        $this->_imageUploader = $imageUploader;
        $this->_brandsCollection = $collectionFactory;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }
    
    /**
     * @return \Clicks\Grossentabelle\Model\ResourceModel\Brands\Collection
     */
    public function getBrands()
    {
        $collection = $this->_brandsCollection->create();
        
        return $collection->addFieldToFilter('status', 1);
    }
    
    /**
     * @return string
     */
    public function getBaseImagePath()
    {
        return $this->_imageUploader->baseTmpPath;
    }
    
    /**
     * @return \Magento\Framework\View\Element\Template
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _prepareLayout()
    {
        /** @var \Magento\Theme\Block\Html\Breadcrumbs $breadcrumbs */
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        $brandsUrl = $this->_scopeConfig->getValue('brands_fallback/config/url', ScopeInterface::SCOPE_STORE);
    
        // generate dynamic breadcrumbs
        $breadcrumbs
            ->addCrumb('Home', ['link' => '/', 'label' => 'Home', 'title'=> 'Home'])
            ->addCrumb('Brands', ['link' => $brandsUrl, 'label' => __('Marken'), 'title'=> __('Marken')])
        ;
        $this->getLayout()->setBlock('breadcrumbs', $breadcrumbs);
        
        return parent::_prepareLayout();
    }
}