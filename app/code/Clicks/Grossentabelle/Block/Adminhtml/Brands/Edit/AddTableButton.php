<?php

namespace Clicks\Grossentabelle\Block\Adminhtml\Brands\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class AddTableButton extends Button implements ButtonProviderInterface
{
    /**
     * Retrieve button-specified settings
     *
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Add Table'),
            'config' => [
                'actions' => [
                    'targetName' => '${ $.parentName}.test_modal',
                    'actionName' => 'openModal'
                ],
            ],
            'sort_order' => 90,
            'url' => $this->getAddTableUrl()
        ];
    }
    
    /**
     * Get URL to add table to this brand
     *
     * @return string
     */
    public function getAddTableUrl()
    {
        return $this->getUrl('*/brandstables/addbrandtable', ['brand_id' => $this->getModelId()]);
    }
    
}