<?php

namespace Clicks\Grossentabelle\Block\Adminhtml\BrandsTables\Edit;

use Magento\Backend\Block\Widget\Context;

abstract class Button
{
    protected $context;
    
    public function __construct(Context $context)
    {
        $this->context = $context;
    }
    
    /**
     * Return model ID
     *
     * @return int|null
     */
    public function getModelId()
    {
        return $this->context->getRequest()->getParam('id');
    }
    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}