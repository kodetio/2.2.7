<?php

namespace Clicks\Grossentabelle\Controller;

use Clicks\Grossentabelle\Model\BrandsRepository;
use Clicks\Grossentabelle\Model\ResourceModel\Brands;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Router implements RouterInterface
{
    private $_actionFactory;
    
    private $_brandsCollection;
    
    private $_brandsRepository;
    
    private $_scopeConfig;
    
    public function __construct(
        ActionFactory $actionFactory,
        Brands\Collection $brandsCollection,
        BrandsRepository $brandsRepository,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->_actionFactory = $actionFactory;
        $this->_brandsRepository = $brandsRepository;
        $brandsCollectionArray = $brandsCollection
            ->addFieldToSelect('id')
            ->addFieldToSelect('url')
            ->toArray();
        
        $this->_scopeConfig = $scopeConfig;
        
        foreach ($brandsCollectionArray['items'] as $index => $arrayWithValues) {
            $this->_brandsCollection['/'.$arrayWithValues['url']] = $arrayWithValues['id'];
        }
    }
    
    public function match(RequestInterface $request)
    {
        $info = $request->getPathInfo();
        // renders brands details page
        if (array_key_exists($info, $this->_brandsCollection)) {
            $request->setPathInfo('/');
            /** @var \Clicks\Grossentabelle\Model\Brands $brand */
            $brand = $this->_brandsRepository->getById($this->_brandsCollection[$info]);
            $request->setParams(
                [
                    'brand'=> $brand
                ]
            );
            
            return $this->_actionFactory->create(
                'Clicks\Grossentabelle\Controller\SizeTable',
                [
                    'request' => $request,
                ]
            );
        }
        
        $brandsUrl = $this->_scopeConfig->getValue('brands_fallback/config/url', ScopeInterface::SCOPE_STORE);
        // renders brands fallback page
        if ($info === '/'.$brandsUrl) {
            return $this->_actionFactory->create(
                'Clicks\Grossentabelle\Controller\BrandsOverview',
                [
                    'request' => $request,
                ]
            );
        }
        
        return null;
    }
}