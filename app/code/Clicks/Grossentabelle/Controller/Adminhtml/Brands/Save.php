<?php

namespace Clicks\Grossentabelle\Controller\Adminhtml\Brands;

use Clicks\Grossentabelle\Api\BrandsRepositoryInterface;
use Clicks\Grossentabelle\Controller\Adminhtml\Brands;
use Magento\Backend\App\Action\Context;
use Clicks\Grossentabelle\Model\BrandsFactory;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Registry;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool as FrontendPool;
use Magento\Framework\Stdlib\DateTime\DateTime;
use phpDocumentor\Reflection\Types\Integer;


class Save extends Brands
{
    /**
     * @var BrandsFactory
     */
    protected $_brandsFactory;
    /**
     * @var TypeListInterface
     */
    protected $_cacheTypeList;
    /**
     * @var FrontendPool
     */
    protected $_cacheFrontendPool;
    /**
     * @var DateTime
     */
    protected $_dateTime;
    
    /**
     * @var \Clicks\Grossentabelle\Api\BrandsRepositoryInterface
     */
    private $_brandsRepository;
    
    /** @var \Magento\Framework\App\ResourceConnection  */
    private $_resource;
    
    private $_tableName;
    
    /**
     * Save constructor.
     *
     * @param \Magento\Backend\App\Action\Context                  $context
     * @param \Magento\Framework\Registry                          $coreRegistry
     * @param \Clicks\Grossentabelle\Model\BrandsFactory           $brandsFactory
     * @param \Magento\Framework\App\Cache\TypeListInterface       $cacheTypeList
     * @param \Magento\Framework\App\Cache\Frontend\Pool           $cacheFrontendPool
     * @param \Magento\Framework\Stdlib\DateTime\DateTime          $dateTime
     * @param \Clicks\Grossentabelle\Api\BrandsRepositoryInterface $brandsRepository
     * @param \Magento\Framework\App\ResourceConnection            $resource
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        BrandsFactory $brandsFactory,
        TypeListInterface $cacheTypeList,
        FrontendPool $cacheFrontendPool,
        DateTime $dateTime,
        BrandsRepositoryInterface $brandsRepository,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->_brandsFactory = $brandsFactory;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->_dateTime = $dateTime;
        $this->_brandsRepository = $brandsRepository;
        $this->_resource = $resource;
        parent::__construct($context, $coreRegistry);
    }
    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // retrieves all $POST values from 'brand' array
        $data = $this->getRequest()->getPostValue('brand');

        if ($data) {
            $id = array_key_exists('id', $data) ? $data['id'] : null;

            if ($id) {
                $brand = $this->_brandsRepository->getById($id);
            } else {
                $brand = $this->_brandsFactory->create();
            }
            
            $brand->setName($data['name'])
                ->setStatus($data['status'])
                ->setDescription($data['description'])
                ->setUrl($data['url'])
                ->setLogo($data['graphics'][0]['name'])
                ;
            
            try {
                $this->_brandsRepository->save($brand);
                $this->messageManager->addSuccessMessage(__('Successfully saved new brand'));
                $this->_cacheTypeList->cleanType('config');

                foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                    $cacheFrontend->getBackend()->clean();
                }

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['brand_id' => $brand->getId()]);
                }

                return $resultRedirect->setPath('*/*/');

            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving brand.'));
            }

            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
