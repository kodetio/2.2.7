<?php

namespace Clicks\Grossentabelle\Controller\Adminhtml\Brands;

use Clicks\Grossentabelle\Model\CsvUploader;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;

class CsvUpload extends Action
{
    public $imageUploader;
    
    public function __construct(
        Action\Context $context,
        CsvUploader $imageUploader
    ) {
        $this->imageUploader = $imageUploader;
        parent::__construct($context);
    }
    
    public function execute()
    {
        try {
            $fileId = $this->getRequest()->getFiles('brand');
            $result = $this->imageUploader->saveFileToTmpDir($fileId['csv']);
            $result['cookie'] = [
                'name' => $this->_getSession()->getName(),
                'value' => $this->_getSession()->getSessionId(),
                'lifetime' => $this->_getSession()->getCookieLifetime(),
                'path' => $this->_getSession()->getCookiePath(),
                'domain' => $this->_getSession()->getCookieDomain(),
            ];
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}