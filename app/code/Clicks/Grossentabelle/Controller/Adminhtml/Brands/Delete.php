<?php

namespace Clicks\Grossentabelle\Controller\Adminhtml\Brands;

use Clicks\Grossentabelle\Model\BrandsFactory;
use Clicks\Grossentabelle\Controller\Adminhtml\Brands;
use Clicks\Grossentabelle\Model\BrandsRepository;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Delete extends Brands
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    
    /**
     * @var \Clicks\Grossentabelle\Model\BrandsFactory
     */
    protected $_brandsFactory;
    
    /** @var \Clicks\Grossentabelle\Model\BrandsRepository $_brandsRepository */
    protected $_brandsRepository;
    
    /**
     * Edit constructor.
     *
     * @param \Magento\Backend\App\Action\Context           $context
     * @param \Magento\Framework\Registry                   $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory    $resultPageFactory
     * @param \Clicks\Grossentabelle\Model\BrandsFactory    $brandsFactory
     * @param \Clicks\Grossentabelle\Model\BrandsRepository $brandsRepository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        BrandsFactory $brandsFactory,
        BrandsRepository $brandsRepository
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_brandsFactory = $brandsFactory;
        $this->_brandsRepository = $brandsRepository;
        parent::__construct($context, $coreRegistry);
    }
    
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
    
        if (!($contact = $this->_objectManager->create(\Clicks\Grossentabelle\Model\Brands::class)->load($id))) {
            $this->messageManager->addErrorMessage(__('Unable to proceed. Please, try again.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }
        try{
            $contact->delete();
            $this->messageManager->addSuccessMessage(__('Marke wurde efrolgreich gelöscht'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Fehler bei Marke löschen: '));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }
    
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index', array('_current' => true));
    }
}