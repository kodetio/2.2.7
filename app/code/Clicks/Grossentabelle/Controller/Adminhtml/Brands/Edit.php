<?php

namespace Clicks\Grossentabelle\Controller\Adminhtml\Brands;

use Clicks\Grossentabelle\Model\BrandsFactory;
use Clicks\Grossentabelle\Controller\Adminhtml\Brands;
use Clicks\Grossentabelle\Model\BrandsRepository;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends Brands
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    
    /**
     * @var \Clicks\Grossentabelle\Model\BrandsFactory
     */
    protected $_brandsFactory;
    
    /** @var \Clicks\Grossentabelle\Model\BrandsRepository $_brandsRepository */
    protected $_brandsRepository;
    
    /**
     * Edit constructor.
     *
     * @param \Magento\Backend\App\Action\Context           $context
     * @param \Magento\Framework\Registry                   $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory    $resultPageFactory
     * @param \Clicks\Grossentabelle\Model\BrandsFactory    $brandsFactory
     * @param \Clicks\Grossentabelle\Model\BrandsRepository $brandsRepository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        BrandsFactory $brandsFactory,
        BrandsRepository $brandsRepository
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_brandsFactory = $brandsFactory;
        $this->_brandsRepository = $brandsRepository;
        parent::__construct($context, $coreRegistry);
    }
    
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $brand = $this->_brandsFactory->create();
        if ($id) {
            $brand = $this->_brandsRepository->getById($id);
            if (!$brand->getId()) {
                $this->messageManager->addErrorMessage(__('This brand no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('brands', $brand);
        $resultPage = $this->_resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit brand') : __('New brand'),
            $id ? __('Edit brand') : __('New brand')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Brands'));
        $resultPage->getConfig()->getTitle()->prepend($brand->getId() ? 'Edit brand ' : __('New brand'));
        
        return $resultPage;
    }
}