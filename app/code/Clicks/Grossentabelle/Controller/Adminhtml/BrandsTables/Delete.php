<?php

namespace Clicks\Grossentabelle\Controller\Adminhtml\BrandsTables;

use Clicks\Grossentabelle\Model\BrandsTablesFactory;
use Clicks\Grossentabelle\Controller\Adminhtml\BrandsTables;
use Clicks\Grossentabelle\Model\BrandsTablesRepository;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Delete extends BrandsTables
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    
    /**
     * @var \Clicks\Grossentabelle\Model\BrandsTablesFactory
     */
    protected $_brandsTablesFactory;
    
    /** @var \Clicks\Grossentabelle\Model\BrandsTablesRepository $_brandsTablesRepository */
    protected $_brandsTablesRepository;
    
    /**
     * Edit constructor.
     *
     * @param \Magento\Backend\App\Action\Context           $context
     * @param \Magento\Framework\Registry                   $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory    $resultPageFactory
     * @param \Clicks\Grossentabelle\Model\BrandsTablesFactory    $brandsTablesFactory
     * @param \Clicks\Grossentabelle\Model\BrandsTablesRepository $brandsTablesRepository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        BrandsTablesFactory $brandsTablesFactory,
        BrandsTablesRepository $brandsTablesRepository
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_brandsTablesFactory = $brandsTablesFactory;
        $this->_brandsTablesRepository = $brandsTablesRepository;
        parent::__construct($context, $coreRegistry);
    }
    
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
    
        if (!($contact = $this->_objectManager->create(\Clicks\Grossentabelle\Model\BrandsTables::class)->load($id))) {
            $this->messageManager->addErrorMessage(__('Unable to proceed. Please, try again.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }
        try{
            $contact->delete();
            $this->messageManager->addSuccessMessage(__('Tabelle wurde efrolgreich gelöscht'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Fehler bei Tabelle löschen: '));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', array('_current' => true));
        }
    
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index', array('_current' => true));
    }
}