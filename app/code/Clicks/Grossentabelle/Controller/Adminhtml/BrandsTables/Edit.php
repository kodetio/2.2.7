<?php

namespace Clicks\Grossentabelle\Controller\Adminhtml\BrandsTables;

use Clicks\Grossentabelle\Model\BrandsRepository;
use Clicks\Grossentabelle\Model\BrandsTablesFactory;
use Clicks\Grossentabelle\Controller\Adminhtml\BrandsTables;
use Clicks\Grossentabelle\Model\ResourceModel\Brands;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends BrandsTables
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    
    /**
     * @var \Clicks\Grossentabelle\Model\BrandsTablesFactory
     */
    protected $_brandsTablesFactory;
    
    /** @var \Clicks\Grossentabelle\Model\BrandsRepository $_brandsRepository */
    private $_brandsRepository;
    
    /**
     * Constructor
     *
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultForwardFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        BrandsTablesFactory $brandsTablesFactory,
        BrandsRepository $brandsRepository
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_brandsTablesFactory = $brandsTablesFactory;
        $this->_brandsRepository = $brandsRepository;
        parent::__construct($context, $coreRegistry);
    }
    
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $brandId = $this->getRequest()->getParam('brand_id');
        $model = $this->_brandsTablesFactory->create();
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This brand table no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        
        if ($brandId) {
            $brand = $this->_brandsRepository->getById($brandId);
            if (!$brand->getId()) {
                $this->messageManager->addErrorMessage(__('This brand no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
            $this->_coreRegistry->register('brand', $brand);
        }
        
        $this->_coreRegistry->register('brandstables', $model);
        
        $resultPage = $this->_resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit table') : __('New size table'),
            $id ? __('Edit table') : __('New size table')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Tables'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? 'Edit table ' : __('New table'));
        return $resultPage;
    }
}