<?php

namespace Clicks\Grossentabelle\Controller\Adminhtml\BrandsTables;

use Clicks\Grossentabelle\Model\BrandsTablesRepository;
use Clicks\Grossentabelle\Controller\Adminhtml\BrandsTables;
use Magento\Backend\App\Action\Context;
use Clicks\Grossentabelle\Model\BrandsTablesFactory;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Registry;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool as FrontendPool;
use Magento\Framework\Stdlib\DateTime\DateTime;


class Save extends BrandsTables
{
   /** @var \Clicks\Grossentabelle\Model\BrandsTablesFactory $brandsTablesFactory */
    protected $brandsTablesFactory;
    /**
     * @var TypeListInterface
     */
    protected $_cacheTypeList;
    /**
     * @var FrontendPool
     */
    protected $_cacheFrontendPool;
    /**
     * @var DateTime
     */
    protected $_dateTime;
    
    /** @var \Clicks\Grossentabelle\Model\BrandsTablesRepository $brandsTablesRepository */
    private $brandsTablesRepository;
    
    /** @var \Magento\Framework\App\ResourceConnection  */
    private $_resource;
    
    /** @var string */
    private $_tableName;
    
    /** @var \Magento\Framework\File\Csv $_csv */
    private $_csv;
    
    /**
     * Save constructor.
     *
     * @param \Magento\Backend\App\Action\Context                        $context
     * @param \Magento\Framework\Registry                                $coreRegistry
     * @param \Clicks\Grossentabelle\Model\BrandsTablesFactory           $brandsTablesFactory
     * @param \Magento\Framework\App\Cache\TypeListInterface             $cacheTypeList
     * @param \Magento\Framework\App\Cache\Frontend\Pool                 $cacheFrontendPool
     * @param \Magento\Framework\Stdlib\DateTime\DateTime                $dateTime
     * @param \Clicks\Grossentabelle\Model\BrandsTablesRepository $brandsTablesRepository
     * @param \Magento\Framework\App\ResourceConnection                  $resource
     * @param \Magento\Framework\File\Csv                                $csv
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        BrandsTablesFactory $brandsTablesFactory,
        TypeListInterface $cacheTypeList,
        FrontendPool $cacheFrontendPool,
        DateTime $dateTime,
        BrandsTablesRepository $brandsTablesRepository,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\File\Csv $csv
    ) {
        $this->brandsTablesFactory = $brandsTablesFactory;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->_dateTime = $dateTime;
        $this->brandsTablesRepository = $brandsTablesRepository;
        $this->_resource = $resource;
        $this->_csv = $csv;
        parent::__construct($context, $coreRegistry);
    }
    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // retrieves all $POST values from 'brand' array
        $data = $this->getRequest()->getPostValue('table');

        if ($data) {
            $id = array_key_exists('id', $data) ? $data['id'] : null;

            if ($id) {
                /** @var \Clicks\Grossentabelle\Model\BrandsTables $brandsTables */
                $brandsTables = $this->brandsTablesRepository->getById($id);
            } else {
                $brandsTables = $this->brandsTablesFactory->create();
            }
            
            $brandsTables->setName($data['name'])
                ->setSortOrder($data['sort_order'])
                ->setInfotext($data['infotext'])
                ->setGraphicsInfotext($data['graphics_infotext'])
                ->setDescription($data['description'])
                ->setAttributeMapping($data['attribute_mapping'])
                ->setAnchor($data['anchor'])
                ->setBrandId($data['brand_id'])
                ->setGraphics($this->_getGraphics());
            
            try {
                $this->brandsTablesRepository->save($brandsTables);
                $this->_setTableName($brandsTables->getId());
                $this->_processCsvData($data['csv'][0]);

                $this->messageManager->addSuccessMessage(__('Successfully saved!'));
                $this->_cacheTypeList->cleanType('config');

                foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                    $cacheFrontend->getBackend()->clean();
                }

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['table_id' => $brandsTables->getId()]);
                }

                return $resultRedirect->setPath('*/*/');

            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving brand.'));
            }

            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
    
    private function _getGraphics()
    {
        $postData = $this->getRequest()->getParams();
        if (array_key_exists('graphics', $postData)) {
            return $postData['graphics'][0]['name'];
        }
        
        return null;
    }
    
    private function _processCsvData($csvFilePath)
    {
        try {
        
            $file = $csvFilePath['path'].'/'.$csvFilePath['file'];
            $this->_csv->setDelimiter(';');
            $csvData = $this->_csv->getData($file);
        
            // take number of columns from the first row
            $this->_createTable(count($csvData[0]));
            
            // insert data into table
            $this->_insertValues($csvData);
            
        } catch (\Exception $exception) {
            die($exception->getMessage());
        }
    }
    
    private function _createTable($columnCount)
    {
        $setup = $this->_resource;
        $connection = $setup->getConnection();
        
        // cleanup previous table if exists
        if ($connection->isTableExists($this->_getTableName())) {
            $connection->dropTable($this->_getTableName());
        }

        // creates new table
        if (!$connection->isTableExists($this->_getTableName())) {
            $table = $connection->newTable(
                $this->_getTableName()
            );
            
            // creates table with defined number of columns
            for ($index = 1; $index <= $columnCount; $index++) {
                $table->addColumn(
                    $index,
                    Table::TYPE_TEXT,
                    255
                );
            }

            $connection->createTable($table);
        }
    }
    
    private function _insertValues($values)
    {
        $setup = $this->_resource;
        $connection = $setup->getConnection();
        
        $table = $connection->insertArray($this->_getTableName(), range(1, count($values[0])), $values);
    }
    
    /**
     * @param string $brandId
     */
    private function _setTableName($brandId)
    {
        $this->_tableName = 'clicks_grossentabelle_brands_tables_'.$brandId.'_values';
    }

    private function _getTableName()
    {
        return $this->_tableName;
    }
}
