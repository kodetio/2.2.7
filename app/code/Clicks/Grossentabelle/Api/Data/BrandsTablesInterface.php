<?php

namespace Clicks\Grossentabelle\Api\Data;

interface BrandsTablesInterface
{
    const ID = 'id';
    const BRAND_ID = 'brand_id';
    const NAME = 'name';
    const ANCHOR = 'anchor';
    const SORT_ORDER = 'sort_order';
    const DESCRIPTION = 'description';
    const INFOTEXT = 'infotext';
    const GRAPHICS = 'graphics';
    const GRAPHICS_INFOTEXT = 'graphics_infotext';
    const ATTRIBUTE_MAPPING = 'attribute_mapping';
    
    public function getId();
    public function getName();
    public function getAnchor();
    public function getSortOrder();
    public function getDescription();
    public function getInfotext();
    public function getGraphics();
    public function getGraphicsInfotext();
    public function getAttributeMapping();
    public function getBrandId();
    public function getValues();
    
    /**
     * @param int $id
     *
     * @return mixed
     */
    public function setId($id);
    
    /**
     * @param string $name
     *
     * @return mixed
     */
    public function setName($name);
    
    /**
     * @param string $anchor
     *
     * @return mixed
     */
    public function setAnchor($anchor);
    
    /**
     * @param int $sortOrder
     *
     * @return mixed
     */
    public function setSortOrder($sortOrder);
    
    /**
     * @param string $description
     *
     * @return mixed
     */
    public function setDescription($description);
    
    /**
     * @param string $infotext
     *
     * @return mixed
     */
    public function setInfotext($infotext);
    
    /**
     * @param string $graphics
     *
     * @return mixed
     */
    public function setGraphics($graphics);
    
    /**
     * @param string $graphicsInfotext
     *
     * @return mixed
     */
    public function setGraphicsInfotext($graphicsInfotext);
    
    /**
     * @param string $attributeMapping
     *
     * @return mixed
     */
    public function setAttributeMapping($attributeMapping);
    
    /**
     * @param int $brandId
     *
     * @return mixed
     */
    public function setBrandId($brandId);
}