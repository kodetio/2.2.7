<?php

namespace Clicks\Grossentabelle\Api\Data;

interface BrandsInterface
{
    const ID = 'id';
    const STATUS = 'status';
    const NAME = 'name';
    const URL = 'url';
    const LOGO = 'logo';
    const DESCRIPTION = 'description';
    
    public function getId();
    public function getStatus();
    public function getName();
    public function getUrl();
    public function getLogo();
    public function getDescription();
    public function getTables();
    
    /**
     * @param int $id
     *
     * @return mixed
     */
    public function setId($id);
    
    /**
     * @param bool $status
     *
     * @return mixed
     */
    public function setStatus($status);
    
    /**
     * @param string $name
     *
     * @return mixed
     */
    public function setName($name);
    
    /**
     * @param string $url
     *
     * @return mixed
     */
    public function setUrl($url);
    
    /**
     * @param string $logo
     *
     * @return mixed
     */
    public function setLogo($logo);
    
    /**
     * @param string $description
     *
     * @return mixed
     */
    public function setDescription($description);
}