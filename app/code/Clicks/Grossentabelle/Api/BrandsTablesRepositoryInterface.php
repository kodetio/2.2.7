<?php

namespace Clicks\Grossentabelle\Api;

use Clicks\Grossentabelle\Api\Data\BrandsTablesInterface;

interface BrandsTablesRepositoryInterface
{
    public function save(BrandsTablesInterface $brandsTables);
    public function getById($brandId);
    public function delete(BrandsTablesInterface $brand);
    public function deleteById($brandId);
}