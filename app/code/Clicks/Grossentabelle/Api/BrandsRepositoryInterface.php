<?php

namespace Clicks\Grossentabelle\Api;

use Clicks\Grossentabelle\Api\Data\BrandsInterface;

interface BrandsRepositoryInterface
{
    public function save(BrandsInterface $brand);
    public function getById($brandId);
    public function delete(BrandsInterface $brand);
    public function deleteById($brandId);
}