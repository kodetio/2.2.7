<?php

namespace Clicks\Grossentabelle\Model;

use Clicks\Grossentabelle\Api\Data\BrandsInterface;
use Magento\Framework\Model\AbstractModel;
use Clicks\Grossentabelle\Model\ResourceModel\Brands as BrandsResource;

class Brands extends AbstractModel implements BrandsInterface {
    
    public function getId() {
        return $this->_getData(self::ID);
    }
    
    public function getName() {
        return $this->_getData(self::NAME);
    }
    
    public function getLogo()
    {
        return $this->_getData(self::LOGO);
    }
    
    public function getStatus()
    {
        return $this->_getData(self::STATUS);
    }
    
    public function getDescription() {
        return $this->_getData(self::DESCRIPTION);
    }
    
    public function getUrl()
    {
        return $this->_getData(self::URL);
    }
    
    public function getTables()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var \Clicks\Grossentabelle\Model\ResourceModel\Brands\Collection $brandsTablesCollection */
        $brandsTablesCollection = $objectManager->get('Clicks\Grossentabelle\Model\ResourceModel\BrandsTables\Collection');
        
        return $brandsTablesCollection
            ->addFieldToFilter('brand_id', $this->getId())
            ->setOrder('sort_order')
            ->load();
    }
    
    /**
     * {@inheritDoc}
     */
    public function setId($id) {
        return $this->setData(self::ID, $id);
    }
    
    /**
     * {@inheritDoc}
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }
    
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }
    
    public function setLogo($logo)
    {
        return $this->setData(self::LOGO, $logo);
    }
    
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }
    
    public function setUrl($url)
    {
        return $this->setData(self::URL, $url);
    }
    
    /**
     * {@inheritdoc}
     */
    protected function _construct() {
        $this->_init(\Clicks\Grossentabelle\Model\ResourceModel\Brands::class);
    }
    
}