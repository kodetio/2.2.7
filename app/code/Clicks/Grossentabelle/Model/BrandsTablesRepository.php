<?php

namespace Clicks\Grossentabelle\Model;

use Clicks\Grossentabelle\Api\Data\BrandsTablesInterface;
use Clicks\Grossentabelle\Api\BrandsTablesRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class BrandsTablesRepository implements BrandsTablesRepositoryInterface
{
    private $_resourceBrandsTables;
    
    private $_brandsTablesFactory;
    
    public function __construct(
        ResourceModel\BrandsTables $resourceBrands,
        \Clicks\Grossentabelle\Api\Data\BrandsTablesInterfaceFactory $brandsTables
    ) {
        $this->_brandsTablesFactory = $brandsTables;
        $this->_resourceBrandsTables = $resourceBrands;
    }
    
    public function save(BrandsTablesInterface $brandsTables)
    {
        try {
            $this->_resourceBrandsTables->save($brandsTables);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        
        return $brandsTables;
    }
    
    public function getById($brandId)
    {
        $brand = $this->_brandsTablesFactory->create();
        $this->_resourceBrandsTables->load($brand, $brandId);
        if (!$brand->getId()) {
            throw new NoSuchEntityException(__('Brand with id "%1" dos not exists', $brandId));
        }
        
        return $brand;
    }
    
    public function delete(BrandsTablesInterface $brand)
    {
        try {
            $this->_resourceBrandsTables->delete($brand);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
    }
    
    public function deleteById($brandId)
    {
        return $this->delete($this->getById($brandId));
    }
}