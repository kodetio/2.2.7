<?php

namespace Clicks\Grossentabelle\Model\Brands;

use Clicks\Grossentabelle\Model\ResourceModel\Brands;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Registry;

class SelectSource implements OptionSourceInterface
{
    /**
     * @var null|array
     */
    protected $options;
    
    /** @var \Magento\Framework\Registry $_registry */
    private $_registry;
    
    /** @var \Clicks\Grossentabelle\Model\ResourceModel\Brands\CollectionFactory $_collectionFactory */
    private $_collectionFactory;
    
    /**
     * SelectSource constructor.
     *
     * @param \Clicks\Grossentabelle\Model\ResourceModel\Brands\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Registry                                         $coreRegistry
     */
    public function __construct(
        \Clicks\Grossentabelle\Model\ResourceModel\Brands\CollectionFactory $collectionFactory,
        Registry $coreRegistry
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_registry = $coreRegistry;
    }
    
    /**
     * @return array|null
     */
    public function toOptionArray()
    {
        if (null === $this->options) {
            $this->options = $this
                ->_collectionFactory
                ->create();

            if ($this->_registry->registry('brand') !== null) {
                $this->addFieldToFilter('id', $this->_registry->registry('brand')->getId());
            }
            
        }

        return $this->options->toOptionArray();
    }
}
