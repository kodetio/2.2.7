<?php

namespace Clicks\Grossentabelle\Model\Brands;

use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Psr\Log\LoggerInterface;

class DataProvider extends AbstractDataProvider
{
    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Clicks\Grossentabelle\Model\ResourceModel\Brands\CollectionFactory $brandsCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Clicks\Grossentabelle\Model\ResourceModel\Brands\CollectionFactory $brandsCollectionFactory,
        StoreManagerInterface $storeManager,
        LoggerInterface $logger,
        array $meta = [],
        array $data = []
    )
    {
        $this->storeManager = $storeManager;
        $this->_logger = $logger;
        $this->collection = $brandsCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }
    
    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        $this->loadedData = array();
        foreach ($items as $item) {
            $this->loadedData[$item->getId()]['brand'] = $item->getData();
            // a bit ugly hack to bypass UI component limits for editing
            $this->loadedData[$item->getId()]['brand']['logo'] = null;
        }
        return $this->loadedData;
    }
    
    /**
     * @return string
     */
    public function getMediaUrl()
    {
        try {
            $mediaUrl = $this->storeManager->getStore()
                    ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA).'grossentabelle/tmp/upload/';
            return $mediaUrl;
        } catch (\Exception $exception) {
            $this->_logger->error($exception->getMessage());
        }
    }
}