<?php

namespace Clicks\Grossentabelle\Model\BrandsTables;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\UrlInterface;
use Psr\Log\LoggerInterface;

class DataProvider extends AbstractDataProvider
{
    /** @var \Magento\Store\Model\StoreManagerInterface $storeManager */
    public $storeManager;
    
    /** @var \Psr\Log\LoggerInterface $_logger */
    private $_logger;
    
    /**
     * DataProvider constructor.
     *
     * @param                                                                           $name
     * @param                                                                           $primaryFieldName
     * @param                                                                           $requestFieldName
     * @param \Clicks\Grossentabelle\Model\ResourceModel\BrandsTables\CollectionFactory $brandsCollectionFactory
     * @param array                                                                     $meta
     * @param array                                                                     $data
     * @param \Magento\Store\Model\StoreManagerInterface                                $storeManager
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Clicks\Grossentabelle\Model\ResourceModel\BrandsTables\CollectionFactory $brandsCollectionFactory,
        StoreManagerInterface $storeManager,
        LoggerInterface $logger,
        array $meta = [],
        array $data = []
    )
    {
        $this->collection = $brandsCollectionFactory->create();
        $this->storeManager = $storeManager;
        $this->_logger = $logger;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }
    
    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        $this->loadedData = [];
        /** @var \Clicks\Grossentabelle\Model\BrandsTables $item */
        foreach ($items as $item) {
            $this->loadedData[$item->getId()]['table'] = $item->getData();
            // a bit ugly hack to bypass UI component limits for editing
            $this->loadedData[$item->getId()]['table']['csv'] = null;
            $this->loadedData[$item->getId()]['table']['graphics'] = null;
        }
        return $this->loadedData;
    }
    
    /**
     * @return string
     */
    public function getMediaUrl()
    {
        try {
            $mediaUrl = $this->storeManager->getStore()
                    ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA).'grossentabelle/tmp/upload/';
            return $mediaUrl;
        } catch (\Exception $exception) {
            $this->_logger->error($exception->getMessage());
        }
    }
}