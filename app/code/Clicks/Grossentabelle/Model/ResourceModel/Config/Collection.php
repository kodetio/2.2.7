<?php

namespace Clicks\Grossentabelle\Model\ResourceModel\Config;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'clicks_grossentabelle_config_collection';
    protected $_eventObject = 'config_collection';
    
    protected function _construct()
    {
        $this->_init(
            'Clicks\Grossentabelle\Model\Config',
            'Clicks\Grossentabelle\Model\ResourceModel\Config'
        );
    }
}