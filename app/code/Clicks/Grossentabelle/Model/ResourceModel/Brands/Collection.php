<?php

namespace Clicks\Grossentabelle\Model\ResourceModel\Brands;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'clicks_grossentabelle_brands_collection';
    protected $_eventObject = 'brands_collection';
    
    protected function _construct()
    {
        $this->_init(
            'Clicks\Grossentabelle\Model\Brands',
            'Clicks\Grossentabelle\Model\ResourceModel\Brands'
        );
    }
}