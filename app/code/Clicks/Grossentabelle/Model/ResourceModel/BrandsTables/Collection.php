<?php

namespace Clicks\Grossentabelle\Model\ResourceModel\BrandsTables;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'clicks_grossentabelle_brands_tables_collection';
    protected $_eventObject = 'brands_tables_collection';
    
    protected function _construct()
    {
        $this->_init(
            'Clicks\Grossentabelle\Model\BrandsTables',
            'Clicks\Grossentabelle\Model\ResourceModel\BrandsTables'
        );
    }
}