<?php

namespace Clicks\Grossentabelle\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class Brands extends AbstractDb
{
    public function __construct(
        Context $context,
        $connectionName = NULL
    ) {
        parent::__construct($context, $connectionName);
    }
    
    protected function _construct() {
        $this->_init('clicks_grossentabelle_brands', 'id');
    }
}