<?php

namespace Clicks\Grossentabelle\Model;

use Clicks\Grossentabelle\Api\Data\BrandsTablesInterface;
use Magento\Framework\Model\AbstractModel;

class BrandsTables extends AbstractModel implements BrandsTablesInterface {
    
    public function getId() {
        return $this->_getData(self::ID);
    }
    
    public function getBrandId()
    {
        return $this->_getData(self::BRAND_ID);
    }
    
    public function getName() {
        return $this->_getData(self::NAME);
    }
    
    public function getAnchor() {
        return $this->_getData(self::ANCHOR);
    }
    
    public function getAttributeMapping() {
        return $this->_getData(self::ATTRIBUTE_MAPPING);
    }
    
    public function getDescription() {
        return $this->_getData(self::DESCRIPTION);
    }
    
    public function getGraphics() {
        return $this->_getData(self::GRAPHICS);
    }
    
    public function getGraphicsInfotext() {
        return $this->_getData(self::GRAPHICS_INFOTEXT);
    }
    
    public function getInfotext() {
        return $this->_getData(self::INFOTEXT);
    }
    
    public function getSortOrder() {
        return $this->_getData(self::SORT_ORDER);
    }
    
    public function getValues()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        /** @var \Magento\Framework\App\ResourceConnection $resource */
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        if ($connection->isTableExists('clicks_grossentabelle_brands_tables_'.$this->getId().'_values')) {
            
            $select = $connection->select()
                ->from('clicks_grossentabelle_brands_tables_'.$this->getId().'_values');
            
            return $connection->fetchAll($select);
        }
        
        return [];
    }
    
    /**
     * {@inheritDoc}
     */
    public function setId($id) {
        return $this->setData(self::ID, $id);
    }
    
    /**
     * @param string $name
     *
     * @return \Clicks\Grossentabelle\Model\BrandsTables|mixed
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }
    
    /**
     * @param string $anchor
     *
     * @return \Clicks\Grossentabelle\Model\BrandsTables|mixed
     */
    public function setAnchor($anchor)
    {
        return $this->setData(self::ANCHOR, $anchor);
    }
    
    /**
     * @param string $attributeMapping
     *
     * @return \Clicks\Grossentabelle\Model\BrandsTables|mixed
     */
    public function setAttributeMapping($attributeMapping)
    {
        return $this->setData(self::ATTRIBUTE_MAPPING, $attributeMapping);
    }
    
    /**
     * @param string $description
     *
     * @return \Clicks\Grossentabelle\Model\BrandsTables|mixed
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }
    
    /**
     * @param string $graphics
     *
     * @return \Clicks\Grossentabelle\Model\BrandsTables|mixed
     */
    public function setGraphics($graphics)
    {
        return $this->setData(self::GRAPHICS, $graphics);
    }
    
    /**
     * @param string $graphicsInfotext
     *
     * @return \Clicks\Grossentabelle\Model\BrandsTables|mixed
     */
    public function setGraphicsInfotext($graphicsInfotext)
    {
        return $this->setData(self::GRAPHICS_INFOTEXT, $graphicsInfotext);
    }
    
    /**
     * @param string $infotext
     *
     * @return \Clicks\Grossentabelle\Model\BrandsTables|mixed
     */
    public function setInfotext($infotext)
    {
        return $this->setData(self::INFOTEXT, $infotext);
    }
    
    /**
     * @param int $sortOrder
     *
     * @return \Clicks\Grossentabelle\Model\BrandsTables|mixed
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }
    
    /**
     * @param int $brandId
     *
     * @return \Clicks\Grossentabelle\Model\BrandsTables|mixed
     */
    public function setBrandId($brandId)
    {
        return $this->setData(self::BRAND_ID, $brandId);
    }
    
    /**
     * {@inheritdoc}
     */
    protected function _construct() {
        $this->_init(\Clicks\Grossentabelle\Model\ResourceModel\BrandsTables::class);
    }
    
}