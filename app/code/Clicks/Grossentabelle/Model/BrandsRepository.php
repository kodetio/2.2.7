<?php

namespace Clicks\Grossentabelle\Model;

use Clicks\Grossentabelle\Api\Data\BrandsInterface;
use Clicks\Grossentabelle\Api\BrandsRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Clicks\Grossentabelle\Model\BrandsFactory;

class BrandsRepository implements BrandsRepositoryInterface
{
    private $_resourceBrands;
    
    private $_brandsFactory;
    
    public function __construct(
        ResourceModel\Brands $resourceBrands,
        BrandsFactory $brandsFactory
    ) {
        $this->_brandsFactory = $brandsFactory;
        $this->_resourceBrands = $resourceBrands;
    }
    
    public function save(BrandsInterface $brand)
    {
        try {
            $this->_resourceBrands->save($brand);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        
        return $brand;
    }
    
    public function getById($brandId)
    {
        $brand = $this->_brandsFactory->create();
        $this->_resourceBrands->load($brand, $brandId);
        if (!$brand->getId()) {
            throw new NoSuchEntityException(__('Brand with id "%1" dos not exists', $brandId));
        }
        
        return $brand;
    }
    
    public function delete(BrandsInterface $brand)
    {
        try {
            $this->_resourceBrands->delete($brand);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
    }
    
    public function deleteById($brandId)
    {
        return $this->delete($this->getById($brandId));
    }
}