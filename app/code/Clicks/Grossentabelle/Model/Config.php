<?php

namespace Clicks\Grossentabelle\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;

class Config extends AbstractModel
{
    const CACHE_TAG = 'clicks_grossentabelle_config';
    
    protected $_cacheTag = 'clicks_grossentabelle_config';
    
    protected $_eventPrefix = 'clicks_grossentabelle_config';
    
    public function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = NULL,
        AbstractDb $resourceCollection = NULL,
        array $data = []
    ) {
        $this->_init('Clicks\Grossentabelle\Model\ResourceModel\Config');
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
    
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    public function getDefaultValues()
    {
        $values = [];
        
        return $values;
    }
}