FROM php:7.1-apache

RUN apt-get update \
    && apt-get install -y \
        libmcrypt-dev \
        libxslt1-dev \
        git \
        wget \
        psmisc \
        zlib1g-dev \
        libicu-dev \
        g++ \
        libpng-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
    && apt-get clean\
    && docker-php-ext-configure \
        gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-configure intl \
    && docker-php-ext-install \
        intl \
        gd \
        mbstring \
        pdo_mysql \
        xsl \
        zip \
        opcache \
        bcmath \
        mcrypt \
        xsl \
        intl \
        bcmath \
        soap \
    && chown www-data:www-data /var/www/html -R

RUN a2enmod rewrite && service apache2 restart
